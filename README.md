--
-- Database: `BRI`
--
CREATE DATABASE `BRI` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `BRI`;

-- --------------------------------------------------------

--
-- Table structure for table `at_users`
--

CREATE TABLE IF NOT EXISTS `at_users` (
  `login` varchar(20) NOT NULL,
  `pass` varchar(32) NOT NULL,
  `name` varchar(64) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userlevel` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`,`name`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `quotes`
--

CREATE TABLE IF NOT EXISTS `quotes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quote_number` varchar(255) NOT NULL,
  `salesperson` varchar(4) NOT NULL,
  `date_rec` date NOT NULL,
  `date_submitted` date NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `comments` mediumtext NOT NULL,
  `submitted` tinyint(1) NOT NULL,
  `qty` double NOT NULL,
  `suggested_price` double NOT NULL,
  `quoted_price` double NOT NULL,
  `expected_net_margin` double NOT NULL,
  `po` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;
