<?php 
/*	John Davis
 *  Simple Table Viewer / Editor
 *  9/28/10
 */
session_start();

require_once('Db.php');

require_once("User.php");
$sessiondata = new SessionData;
if (!$sessiondata->logged)
{
	header("Location: login.php");
}

/* You can set these and it will work for any table... */
$col = array('id', 'quote_number', 'salesperson', 'date_rec', 'date_submitted', 'customer_name', 
			 'description', 'comments', 'submitted', 'qty', 'suggested_price', 'quoted_price', 
			 'expected_net_margin', 'po');
$col_names = array('id', 'Quote #', 'Salesperson', 'Date Rec', 'Date Submitted', 'Customer Name', 
			 'Description', 'Comments', 'Submitted', 'QTY', 'Suggested Price', 'Quoted Price', 
			 'Expected Net Margin', 'PO');

$sort_by = $_GET['sort'];
$desc = $_GET['desc'];
if(isset($_GET['query']))
	$query = $_GET['query'];

?>
<html>
<head>
<style type="text/css" media="all"> 
 <!--
 .thead
 { background-color: #95B3D7; font-weight: bold;}
 .thead td a
 {
 	color: black;
 	font-weight: bold;
 	text-decoration: none;
 }
 td
 {
 	border-left: 1px solid black;
 	border-bottom: 1px solid black;
 	padding: 4px 6px 4px 6px;
 }
 tr
 {
 	border-right: 1px solid black; 
 	border-top: 1px solid black;
 }
 table
 {
 	border-right: 1px solid  black;
 	border-top: 1px solid black;
 	 font-size: 12pt;
 	 margin: 0px auto;
 }
 .false
 {
 	background-color: #FFC7CE;
 	color: #9C0006;
 }
 .true
 {
 	background-color:  #C6EFCE;
 	color: #006100;
 }
 body
 { padding: 0px; margin: 0px;}
 #header
 {
 	color: white;
 	font-weight: bold;
 	font-size: 17px;
 	width: 100%;
 	height: 68px;
 	background-color: #95B3D7;
 	vertical-align: middle;
 	padding: 5px;
 }
 form
 { margin: 0px auto; }
 .center
 {
 	width: 100%;
 	text-align:center;
 }
 -->
 </style>
</head>
<body>
<div id="header"><img src="logo.png" /> Basic Resources Inc</div>
<div class="center"><br /><a href="edit.php" style="text-align:center; text-decoration: none; color: black;  display: inline-block;  background: #95B3D7;  border: 2px double black; font-weight: bold;  padding: 4px; width: 75px;">New Quote</a><form method="GET" action="<?php echo $PHP_SELF;?>">
Search:<input type="text" size="12" maxlength="60" name="query"><input type="submit" value="submit" name="submit"></form></div>
<br />
<?php 
		
		$orderby = "ORDER BY ".$col[0];
		
		//set ascending descending
		if($desc > 0)
		{
			$desc = " DESC";
			$desc_link = "&desc=0";
		}
		else
		{
			$desc = "";
			$desc_link = "&desc=1";
		}
		
		//set sort col
		if($sort_by>0) 
		{
			$orderby = "ORDER BY ".$col[$sort_by-1].$desc;
		}
		
		//grab the query and construct the like clause - assume no fulltext search
		if(isset($query))
		{
			$search_query = "WHERE ";
			foreach ( $col as $val)
			{
				$search_query.= $val." LIKE '%".$query."%' OR ";
				
			}
			$search_query .= "0";
		}
		else
		{	
			$search_query = "WHERE 1";
		}
		
		//connect to db
		$db = new Db;
		$db->connect();
		
		$result = $db->query("SELECT * from quotes ".$search_query." ".$orderby);
		
		//echo the results
		echo '<table cellspacing="0" cellpadding="2" border="0">';
		echo '<tr class="thead">';
		
		
		//for each column, make a header cell
		$i = 1;
		foreach ( $col_names as $val)
		{
			echo '<td><a href="?sort='.$i++.$desc_link.'&query='.$query.'">'.$val.'</a></td>'; 
		}
		echo	'</tr>';
		
		//for each result, make a table row
		while($item_info = $db->fetch_row($result))
		{
			echo '<tr>';
			$i = 0;
			foreach ( $col_names as $val)
			{
				
				if($val == "id")
				{
					echo '<td>';
					echo '<a href="edit.php?id='.$item_info[$i++].'" style="font-size: 10px;">Edit</a>';
					echo '</td>';
				}
				else if($val =="Submitted")
				{
				
					echo '<td><b>';
					echo $item_info[$i++] ? 'Submitted' : 'Waiting On';
					echo '</b></td>';
				}
				else if($val =="PO")
				{
					$class = $item_info[$i++] ? 'true' : 'false';
					echo '<td class="'.$class.'">';
					echo $item_info[$i-1] ? 'Y' : 'N';
					echo '</td>';
				}
				else
				{
					echo '<td>';
					echo $item_info[$i++];
					echo '</td>';
				}
			}
			echo '</tr>'; 
		}
		echo '</table>';
?>
</body>
</html>