<?php
#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#
# 
# Copyright (c) 2007 John Eric Davis, all rights reserved.
#
# User.php - v1.0 - J.E.Davis -Feb 24, 2007
#
#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
session_start();
require_once("Db.php");


class SessionData
{
	var $login;
	var $pass;
	var $name;
	var $userlevel;
	var $logged;
	
	function SessionData()
	{
		$this->logged = false;
		
		// If we have it saved in cookies, load that into our session
		if( isset($_COOKIE['clogin']) && isset($_COOKIE['cpass']))
		{
			$_SESSION['login'] = $_COOKIE['clogin'];
			$_SESSION['pass'] = $_COOKIE['cpass'];
		}		
		
		$this->login = $_SESSION['login'];
		$this->pass = $_SESSION['pass'];
		
		if( isset($_SESSION['login']) && isset($_SESSION['pass']))
		{
			if( $this->authenticate($_SESSION['login'], $_SESSION['pass']) )
			{
				$this->logged = true;
				header("Location: index.php");
			}
			else
			{
				unset($_SESSION['login']);
				unset($_SESSION['pass']);
			}
		}		
	}
	
	function authenticate($l, $p)
	{
		$db = new Db;
		$db->connect();
		$result = $db->query("SELECT * from at_users WHERE login = '".$l."'");

		while($row = $db->fetch_row($result))
		{

			if($row[pass] == $p)
			{
				$this->login = $l;
				$this->pass = $p;
				$this->name = $row[name];
				$this->userlevel =$row[userlevel];
				$this->logged = true;
				
				$_SESSION['login'] = $this->login;
				$_SESSION['pass'] = $this->pass;
				
				return;
			}
		}
		$this->logged = false;

	}
	
	function checkUserLevel($level, $redirect)
	{
		if( $level > $userlevel )
			return false;
		//	header("Location: denied.php");
	}
	
	function logout()
	{
		if( $this->logged )
		{
			//expire cookies
			if(isset($_COOKIE['clogin']) && isset($_COOKIE['cpass']))
			{
				setcookie("clogin", "", time()-60*60*24*1, "/");
				setcookie("cpass", "", time()-60*60*24*1, "/");
			}
			
			unset($_SESSION['login']);
			unset($_SESSION['pass']);
			
			$_SESSION = array(); // reset session array
			session_destroy(); // destroy session.
		}	
	}
	function rememberMe()
	{
		setcookie("clogin", $_SESSION['login'], time()+60*60*24*30, "/");
		setcookie("cpass", $_SESSION['pass'], time()+60*60*24*30, "/");	
	}
}



?>
