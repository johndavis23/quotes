<?php 
session_start();

require_once('Db.php');

require_once("User.php");
$sessiondata = new SessionData;
if (!$sessiondata->logged)
{
	header("Location: login.php");
}

/* You should be able to set these and it will work for any table.  Make sure they are in proper order.. */
$col = array('id', 'quote_number', 'salesperson', 'date_rec', 'date_submitted', 'customer_name', 
			 'description', 'comments', 'submitted', 'qty', 'suggested_price', 'quoted_price', 
			 'expected_net_margin', 'po');
$col_names = array('id', 'Quote #', 'Salesperson', 'Date Rec', 'Date Submitted', 'Customer Name', 
			 'Description', 'Comments', 'Submitted', 'QTY', 'Suggested Price', 'Quoted Price', 
			 'Expected Net Margin', 'PO');
$col_type = array('hidden', 'text', 'text', 'text', 'text', 'text', 
			 'text', 'textarea', 'bool', 'text', 'text', 'text', 
			 'text', 'bool');
$col_update = array(0, 1, 1, 1, 1, 1, 
			 1, 1, 1, 1, 1, 1, 
			 1, 1);
$db = new Db;
	$db->connect();
?>
<html>
<head>
<style type="text/css" media="all"> 
 <!--
 .thead
 { background-color: #95B3D7; font-weight: bold;}
 .thead td a
 {
 	color: black;
 	font-weight: bold;
 	text-decoration: none;
 }
 td
 {
 	border-left: 1px solid black;
 	border-bottom: 1px solid black;
 	padding: 4px 6px 4px 6px;
 }
 tr
 {
 	border-right: 1px solid black; 
 	border-top: 1px solid black;
 }
 table
 {
 	border-right: 1px solid  black;
 	border-top: 1px solid black;
 	 font-size: 12pt;
 	 margin: 0px auto;
 }
 .false
 {
 	background-color: #FFC7CE;
 	color: #9C0006;
 }
 .true
 {
 	background-color:  #C6EFCE;
 	color: #006100;
 }
 body
 { padding: 0px; margin: 0px;}
 #header
 {
 	color: white;
 	font-weight: bold;
 	font-size: 17px;
 	width: 100%;
 	height: 68px;
 	background-color: #95B3D7;
 	vertical-align: middle;
 	padding: 5px;
 }
 -->
 </style>
</head>
<body>
<div id="header"><img src="logo.png" /> Basic Resources Inc</div>
<br />
<?php
if(isset($_GET['id']))
{
	$result = $db->query("SELECT * from quotes WHERE id=".$_GET['id']);
	$col_values = array();
	$item_info = $db->fetch_row($result);	
	$i=0;
	foreach($col as $val)
	{
		$col_values[]= $item_info[$i++];
	}
	//we are editing
}
/* 
 * 
 */
	$id = $_GET['id'];

	if (!isset($_POST['submit'])) 
	{

		$i = 0;
		echo '<form method="post" action="'.$PHP_SELF.'">';
		echo '<table cellpadding="0" cellspacing="0" border="0">';
		foreach( $col as $value)
		{
			if($col_type[$i] != "bool")
			{
				echo '<tr><td><label>'.$col_names[$i].':</label></td>'.
				 	 '<td><input type="'.$col_type[$i].'" size="12" maxlength="12" name="'.$value.'" value="'.$col_values[$i].'"></td></tr>';
			}
			else if($col_type[$i] != "textarea")
			{
				echo '<tr><td><label>'.$col_names[$i].':</label></td>';
				echo '<td><textarea rows="3" cols="20">'.$col_values[$i].'</textarea></td></tr>';
			}
			else
			{
				echo '<tr><td><label>'.$col_names[$i].':</label></td>';
				echo '<td><select name="'.$value.'">';
				echo '<option '.($col_values[$i]? 'SELECTED' : '').' value="1">Y</option>';
				echo '<option '.($col_values[$i]? '' : 'SELECTED').' value="0">N</option>';
				echo '</select></td></tr>';
			}
				$i++;
		}
		echo '<tr><td></td><td><input type="submit" value="Save" name="submit"></td></tr>'.
			'</table>'.
			 '</form>';
	}else //page is submitted
	{
		$query = "REPLACE INTO quotes (".implode(", ",$col).") VALUES ('";
		
		$i = 0;
		foreach($col as $value)
		{
			$query .= $_POST[$value]."', '";
		}
		
		$query = substr_replace($query,"",-1);
		$query = substr_replace($query,"",-1);
		$query = substr_replace($query,"",-1);
		$query .= ")";
		//echo $query;
		$result = $db->query($query);
		
		header("Location: /dev/quotes/index.php");
	}
?>

